Certificate Authority
=====================

Documentation : https://openssl-ca.readthedocs.io/en/latest/

Find here some playbooks to make your own CA with OpenSSL.

Self signed
-----------

```sh
ansible-playbook selfsigned.yml
```

Variables :

- `workdir` : Where to puth the files

```sh
ansible-playbook -e workdir=/tmp/my-ca selfsigned.yml
```
